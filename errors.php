<!DOCTYPE html>
<html>
<head>
	<title>Errors</title>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		.page {
			max-width: 50rem;
			margin: 0 auto;
			padding: 1rem;
		}
		.errors {
			/*padding: 1rem;*/
			/*background-color: #fff1ca;*/
		}
		.error {
			margin-bottom: 1.5rem;
		}
		.error__message {
			font-size: 1.25rem;
			padding-bottom: 0.5rem;
			border-bottom: 1px dashed #AAA;
			margin-bottom: 0.75rem;
			color: #962a19;
			margin-bottom: 1rem;
			display: block;
		}
		.error__command {
			padding: 0.85rem 1rem;
			background-color: #fff1ca;
			display: block;
			font-size: 0.75rem;
			margin-bottom: 1rem;
			white-space: pre;
		}

		.trace {
			list-style: none;
			font-size: 0.75rem;
			display: grid;
			grid-gap: 0.5em;
			grid-template-columns: auto 1fr;
		}

		.trace__title {
			align-self: flex-start;
		}
		.trace__list {
			align-self: flex-end;
			padding: 0;
			margin: 0;
		}
		.trace__row {
			list-style: none;
		}

		#copy-input {
			width: 1px;
			height: 1px;
			opacity: 0;
		}
		#copy-button {
			-webkit-appearance: none;
			background-color: #d8d8d8;
			border: 1px dashed transparent;
			color: #6b6b6b;
			width: 20rem;
			font-size: 1rem;
			line-height: 1;
			padding: 1rem;
			transition: all 0.2s;
			outline: none;
		}
		#copy-button:focus {
			border-color: #a5a5a5;
		}
		#copy-button:hover {
			color: #000;
		}
		#copy-button:active {
			border-color: #6b6b6b;
		}
	</style>
</head>
<body>
	<main class="page">
		<header>
			<h1>Errors</h1>
			<button id="copy-button">Copy all commands to clipboard</button>
		</header>
		<div class="errors">
			<?php foreach ( $errors as $error ): ?>
				<div class="error">
					<p class="error__message"><?php echo $error['message']; ?></p>
					<code class="error__command"><?php echo $error['command']; ?></code>
					<?php if ( $error['trace'] ): ?>
						<div class="trace">
							<span class="trace__title">Trace</span>
							<ul class="trace__list">
							<?php foreach ( $error['trace'] as $row ): ?>
								<li class="trace__row">
									<?php if( isset( $row['file'] ) ): ?>
										<span class="file"><?php echo $row['file']; ?></span>
									<?php endif; ?>
									<?php if( isset( $row['line'] ) ): ?>
										<span class="line">:<?php echo $row['line']; ?></span>
									<?php endif; ?>
								</li>
							<?php endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
		<textarea id="copy-input" readonly="readonly"><?php
		foreach ( $errors as $error ) {
			echo "{$error['command']}\n";
		}
		?></textarea>
		<script type="text/javascript">
			var copy_button = document.getElementById('copy-button');
			var copy_input = document.getElementById('copy-input');
			copy_button.onclick = function() {
				copy_input.select();
				if ( document.execCommand( 'copy' ) ) {
					copy_button.innerText = 'Copied!';
				} else {
					copy_button.innerText = 'Error: Could not copy';
				}
				setTimeout( function() {
					copy_button.innerText = 'Copy all commands to clipboard';
				}, 1500 );
			};
		</script>
	</main>
</body>
</html>

