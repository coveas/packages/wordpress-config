<?php
/**
 * WordPress Config
 *
 * @package wp-config
 */
use Dotenv\Dotenv;

// Initialise the Dotenv file.
if ( ! file_exists( $root_path . '/.env' ) ) {
	require __DIR__ . '/instructions.php';
	die;
}

if ( is_callable( '\Dotenv\Dotenv::createImmutable' ) ) {
	$dotenv  = Dotenv::createImmutable( $root_path );
} else if ( is_callable( '\Dotenv\Dotenv::create' ) ) {
	$factory = new Dotenv\Environment\DotenvFactory( [
		new Dotenv\Environment\Adapter\EnvConstAdapter(),
		new Dotenv\Environment\Adapter\PutenvAdapter(),
	] );
	$dotenv  = Dotenv::create( $root_path, null, $factory );
} else {
	$dotenv = new Dotenv( $root_path );
}
$dotenv->load();
$dotenv->required( [
	'APP_NAME',
	'APP_ENV',
	'DB_NAME',
] );

$errors = [];

/**
 * Env function from laravel (Illuminate/Foundation/Helpers)
 *
 * @param  string $key     Key.
 * @param  mixed  $default Default value.
 * @return mixed
 */
function env( string $key, $default = null ) {
	$value = $_ENV[ $key ] ?? $default;

	if ( is_null( $value ) ) {
		return '';
	}

	switch ( strtolower( $value ) ) {
		case 'true':
		case '(true)':
			return true;
		case 'false':
		case '(false)':
			return false;
		case 'empty':
		case '(empty)':
			return '';
		case 'null':
		case '(null)':
			return null;
	}

	if ( strlen( $value ) > 1 && substr( $value, 0, 1 ) === '"' && substr( $value, -1 ) === '"' ) {
		return substr( $value, 1, -1 );
	}
	return $value;
}

if ( file_exists( $root_path . '/init.php' ) ) {
	require_once $root_path . '/init.php';
}

// Application name.
define( 'APP_NAME', env( 'APP_NAME' ) );
if ( ! APP_NAME ) {
	require __DIR__ . '/instructions.php';
	die;
}
define( 'APP_ENV', env( 'APP_ENV', 'production' ) );

// Debug on/off.
$debug = ( 'production' !== APP_ENV );
define( 'WP_DEBUG', env( 'WP_DEBUG', $debug ) );
define( 'SCRIPT_DEBUG', env( 'SCRIPT_DEBUG', $debug ) );

// Local dev on/off.
define( 'WP_LOCAL_DEV', env( 'WP_LOCAL_DEV', ( 'local' === APP_ENV ) ) );

// Disallow file edits on servers.
define( 'DISALLOW_FILE_EDIT', env( 'DISALLOW_FILE_EDIT', ( 'local' !== APP_ENV ) ) );
define( 'DISALLOW_FILE_MODS', env( 'DISALLOW_FILE_MODS', true ) );

if ( WP_DEBUG ) {
	// Run some basic checks on the project.
	$content = file_get_contents( $root_path . '/composer.json' );
	$data    = json_decode( $content, true );
	if ( ! $data ) {
		throw new Exception( 'You have a syntax error in your composer.json file!' );
	}
	if ( ! isset( $data['name'] ) || $data['name'] == 'drivdigital/wordpress-template' ) {
		$errors[] = [
			'message' => 'You need to change the project name in composer.json',
			'command' => "subl composer.json",
			'trace'   => debug_backtrace(),
		];
	}
	if ( APP_NAME == 'application_name_here' ) {
		$errors[] = [
			'message' => 'You need to change the APP_NAME in .env',
			'command' => "subl .env",
			'trace'   => debug_backtrace(),
		];
	}
}

// Define common DB constants.
define( 'DB_NAME', env( 'DB_NAME' ) );
define( 'DB_USER', env( 'DB_USER' ) );
define( 'DB_PASSWORD', env( 'DB_PASSWORD' ) );
define( 'DB_HOST', env( 'DB_HOST', '127.0.0.1' ) );
define( 'DB_CHARSET', env( 'DB_CHARSET', 'utf8' ) );
define( 'DB_COLLATE', env( 'DB_COLLATE', '' ) );
define( 'FS_METHOD', env( 'FS_METHOD', 'direct' ) );

$table_prefix = env( 'DB_PREFIX', 'wp_' );

// Define the rooth path for the website.
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', env( 'ABSPATH', __DIR__ ) );
}

// Parse the current URL.
$ssl      = isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off';
$protocol = ( $ssl ? 'https' : 'http' ) .'://';
$domain   = isset( $_SERVER['HTTP_HOST'] ) ? $_SERVER['HTTP_HOST'] : 'localhost';

// Configure the correct site url's.
define( 'WP_HOME', env( 'WP_HOME', $protocol . $domain ) );
define( 'WP_SITEURL', env( 'WP_SITEURL', $protocol . $domain .'/wp' ) );
define( 'WP_CONTENT_URL', env( 'WP_CONTENT_URL', $protocol . $domain .'/wp-content' ) );

// Set the correct path for wp-content.
define( 'WP_CONTENT_DIR', env( 'WP_CONTENT_DIR', $public_path .'/wp-content/' ) );

// Try to create the uploads directory.
$writable_dirs = [
	'uploads',
	'languages',
	'upgrade',
];

if ( ! function_exists( 'posix_geteuid' ) ) {
	function posix_geteuid() {
		error_log( 'Function `posix_geteuid` is not installed.' );
		return true;
	}
}

if ( is_dir( WP_CONTENT_DIR ) ) {
	$mod = env( 'WP_CONTENT_SUB_MOD', ( APP_ENV === 'production' ? '770' : '777' ) );
	foreach ( $writable_dirs as $dir ) {
		// Only do this if one of the directories is missing.
		$target_dir = WP_CONTENT_DIR . $dir;
		if ( is_dir( $target_dir ) ) {
			continue;
		}
		// Make wp-content writable if the daemon owns the directory.
		if ( ! is_writable( WP_CONTENT_DIR ) && fileowner( WP_CONTENT_DIR ) === posix_geteuid() ) {
			chmod( WP_CONTENT_DIR, octdec( "0{$mod}" ) );
		}
		break;
	}
	// Look for missing or unwritable directories.
	foreach ( $writable_dirs as $dir ) {
		$target_dir = WP_CONTENT_DIR . $dir;
		if ( ! is_dir( $target_dir ) && is_writable( WP_CONTENT_DIR ) ) {
			mkdir( $target_dir );
		}
		if ( ! env( 'IGNORE_UPLOADS_DIR' ) ) {
			if ( ! is_dir( $target_dir ) ) {
				$errors[] = [
					'message' => "The $dir directory has to be created and needs to be writable",
					'command' => "mkdir public/wp-content/$dir &&\nchmod $mod public/wp-content/$dir",
					'trace'   => debug_backtrace(),
				];
			} else if ( ! is_writable( $target_dir ) ) {
				if ( fileowner( $target_dir ) === posix_geteuid() ) {
					chmod( $target_dir, octdec( "0{$mod}" ) );
				}
				if ( ! is_writable( $target_dir ) ) {
					$errors[] = [
						'message' => "The $dir directory needs to be writable",
						'command' => "chmod $mod public/wp-content/$dir",
						'trace'   => debug_backtrace(),
					];
				}
			}
		}
	}
	$mod = env( 'WP_CONTENT_MOD', ( APP_ENV === 'production' ? '570' : '755' ) );
	// Make wp-content non-writable if the daemon owns the directory.
	if ( 'cli' !== php_sapi_name() ) {
		if ( is_writable( WP_CONTENT_DIR ) && fileowner( WP_CONTENT_DIR ) === posix_geteuid() ) {
			chmod( WP_CONTENT_DIR, octdec( "0{$mod}" ) );
		}
		if ( ! is_readable( WP_CONTENT_DIR ) ) {
			array_unshift( $errors, [
				'message' => 'The ' . WP_CONTENT_DIR . ' directory needs to be readable',
				'command' => "chmod $mod public/wp-content",
				'trace'   => debug_backtrace(),
			] );
		}
	}
}

// Reduce the load on the admin.
define( 'WP_POST_REVISIONS', env( 'WP_POST_REVISIONS', 3 ) );
define( 'EMPTY_TRASH_DAYS', env( 'EMPTY_TRASH_DAYS', 7 ) );

// Keys and salts.
define( 'AUTH_KEY', env( 'AUTH_KEY' ) );
define( 'SECURE_AUTH_KEY', env( 'SECURE_AUTH_KEY' ) );
define( 'LOGGED_IN_KEY', env( 'LOGGED_IN_KEY' ) );
define( 'NONCE_KEY', env( 'NONCE_KEY' ) );
define( 'AUTH_SALT', env( 'AUTH_SALT' ) );
define( 'SECURE_AUTH_SALT', env( 'SECURE_AUTH_SALT' ) );
define( 'LOGGED_IN_SALT', env( 'LOGGED_IN_SALT' ) );
define( 'NONCE_SALT', env( 'NONCE_SALT' ) );

$optionals = [
	'LOCAL_INSTALLATION',
	'FORCE_SSL_ADMIN',
	'WP_ALLOW_MULTISITE',
	'MULTISITE',
	'SUBDOMAIN_INSTALL',
	'DOMAIN_CURRENT_SITE',
	'PATH_CURRENT_SITE',
	'SITE_ID_CURRENT_SITE',
	'BLOG_ID_CURRENT_SITE',
	'WP_MEMORY_LIMIT',
	'WPLANG',
	'WP_REDIS_PASSWORD',
	'WP_CACHE_KEY_SALT',
	'WP_CACHE',
	'WP_DEBUG_LOG',
	'WP_DEBUG_DISPLAY',
	'DISABLE_WP_CRON',
];

foreach ( $optionals as $optional ) {
	$value = env( $optional, null );
	if ( null === $value ) {
		continue;
	}
	define( $optional, $value );
}

if ( ! empty( $errors ) ) {
	require __DIR__ . '/errors.php';
	die;
}
